# Gate 403

## Observations

### Game Design

Clearly, the objectives of this project did not include game design aspects. The game design had to be
simple. Its role was to simulate a game.

From my (very amateur) knowledges on Game Design, in order to be sure a game design is good - you need
to test it. You can do this through game prototypes. Once the game design is validated (i.e. the game is
fun), then you can engage more resources.

Here I got my game prototype in few minutes thanks to Bevy. The "game design was okay" - meaning extremly
basic, quickly boring, but good enough for primary objectives (test Rust, Bevy, ECS).

Nonetheless, the development took far more time I initially estimated. (Mainly for good reasons; e.g. I
learned skills with Blender.) During this time, I had the occasion to tweak settings to improve the gameplay.

I was greatly suprised to see how few tweaks in settings could change the "fun level" of a game - by a lot.

_**Example**: the speed for decreasing heat of turrets.  
A value of "0.25", you only have 4-5 shots before overheating your turret and being heavily penalized (not
available for few seconds). It happened accidently quite often - generating misunderstanding, frustrations,
and quickly a "game over" (with again more frustrations).  
However, with a value of "0.45", no more unexpected overheating. You have enough time to see the heat
increasing. You have enough time to switch to the other (cooler) turret._

The interest of having two turrets was initially:  
- Easier shots depending on ork locations
- More firepower (killing more orks/minute)

But now better heat settings bring a new benefit:  
- cooldown management.

This quick win in fun amount is fun to observe.  
I have now more interest for game design (i.e. playing with human mind :) ).

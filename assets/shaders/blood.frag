#version 450

layout(location = 0) in vec2 v_Uv;

layout(location = 0) out vec4 o_Target;

layout(set = 1, binding = 0) uniform BloodMaterial {
    float seconds;
};
layout(set = 1, binding = 1) uniform texture2D BloodMaterial_texture;
layout(set = 1, binding = 2) uniform sampler BloodMaterial_texture_sampler;

const float BRIGHTNESS_OFFSET = 0.0;

void main() {
    vec4 color = texture(
        sampler2D(BloodMaterial_texture, BloodMaterial_texture_sampler),
        v_Uv);
	//if (color.r < seconds * 1.5 || color.a < 0.5) {
	if (color.a < 0.1 || color.a < seconds * 2.0) {// || color.a < 0.5) {
		discard;
	}
	//float a = color.a;
    //float b = (1.0 - color.a) * 0.0;
    //o_Target = vec4((a * color.rgb + b) * 0.5, 1.0);
	o_Target = vec4(color.rgb + vec3(BRIGHTNESS_OFFSET), 1.0);
	//o_Target = vec4(1.0);
}

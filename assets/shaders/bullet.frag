#version 450

layout(location = 0) out vec4 o_Target;
layout(location = 0) in vec3 localPosition;

layout(set = 1, binding = 0) uniform BulletMaterial {
    float seconds;
	vec4 color;
};

void main() {
	float alpha = smoothstep(-0.5, 0.4, localPosition.z);

	float aspect = 4.0 / 6.0;
	float size = 0.6;
	vec2 bullet_size = vec2(size * aspect, size) / 2.0;
	// [-1.0; 1.0]
	vec2 uv = vec2(localPosition.x / (size * aspect), -(localPosition.z / size) * 2.0 - 1.0);
	float dist = 1.0 - smoothstep(0.7, 0.9, length(uv));
	alpha *= dist;

    // Output to screen
    o_Target = vec4(vec3(color), alpha);
}

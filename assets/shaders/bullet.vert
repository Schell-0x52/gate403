#version 450

layout(location = 0) in vec3 Vertex_Position;
layout(set = 0, binding = 0) uniform CameraViewProj {
    mat4 ViewProj;
    mat4 InverseView;
    mat4 Projection;
    vec3 WorldPosition;
    float near;
    float far;
    float width;
    float height;
};

layout(set = 1, binding = 0) uniform BulletMaterial {
    float seconds;
	vec4 color;
};

layout(set = 2, binding = 0) uniform Mesh {
    mat4 Model;
    mat4 InverseTransposeModel;
    uint flags;
};

layout(location = 0) out vec3 localPosition;

void main() {
	vec3 V = Vertex_Position;
	localPosition = V;
	V.x *= 0.4;
	V.z *= max(0.1, min(5.0, seconds*16.0));
	V.z = min(V.z, 1.0);

	V.z += 0.2; // OFFSET!

	vec4 P = Model * vec4(V, 1.0);
    gl_Position = ViewProj * P;
}

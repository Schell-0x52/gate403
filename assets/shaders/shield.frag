#version 450

layout(location = 0) in vec3 vertP;
layout(location = 0) out vec4 o_Target;
layout(set = 1, binding = 0) uniform ShieldMaterial {
    float time;
    float damaging;
    float life;
};

//*/ hexagonal
const float global_scale = 2.0;
const float cellular_base = 1.0;
const float cellular_anim = 0.9;
const float cell_displacement_horizontal = 0.5; //0.0 => square, 0.5 => hexagone
const float cell_displacement_amplitude = 0.08;
const float anim_shadow = 2.0;

const float saturate_field = 0.1;
const float invert_color = 0.1;
const float color_thick = 1.0;
const float color_gradiant = 1.0;

const float colordepth = 8.0;
//*/

/*/ tristonal
const float global_scale = 4.0;
const float cellular_base = 1.0;
const float cellular_anim = 0.9;
const float anim_shadow = 2.0;

const float cell_displacement_horizontal = 0.0;
const float cell_displacement_amplitude = 1.0;

const float saturate_field = 0.1;
const float invert_color = 0.0;
const float color_thick = 1.0;
const float color_gradiant = 1.0;

const float colordepth = 8.0;
//*/


vec2 random2( vec2 p ) {
    return
        fract(
            sin(
                vec2(dot(p,vec2(127.1,311.7)),
                     dot(p,vec2(269.5,183.3)))
            )*43758.5453
        );
}

mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}

float dist_neighbor(vec2 neighbor, vec2 ipart, vec2 fpart) {
	vec2 point = vec2(0.0);
	point.x += mod(ipart.y + neighbor.y, 2.0) * cell_displacement_horizontal;
	vec2 target = 0.5 + 0.5*sin(time * cellular_anim + 6.2831*(random2(neighbor + ipart) + point));
	point = mix(point, target, cell_displacement_amplitude);
	return length(neighbor + point - fpart);
}

float sd_box( in vec2 p, in vec2 b ) {
    vec2 d = abs(p)-b;
    return length(max(d,0.0)) + min(max(d.x,d.y),0.0);
}

float sd_border( in vec2 p ) {
	float rounded = 0.01;
	float border = abs(sd_box(p, vec2(1.0 - rounded)) - rounded);// - 1.0;
	return min(border * 3.0, 1.0);
}

vec3 color_reduce(vec3 c) {
    return floor(c * colordepth) / colordepth;
}


void main()
{
    // "normalized" pixel coordinates (from -1 to 1)
	vec2 uv = vertP.xy;

    uv *= global_scale; // scale

    vec2 uvr = uv; //rotate2d((global_rotation * 3.141592)/180.) * uv;

    // Cellular Noise
    vec2 ipart = floor(uvr);
    vec2 fpart = fract(uvr);

    float min_dist = 3.402823466e+38;
	// note GL ES (2.0?) does not support for loops correctly...
	min_dist = min(min_dist, dist_neighbor(vec2( 1, -1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 1,  0), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 1,  1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 0, -1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 0,  0), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 0,  1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2(-1, -1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2(-1,  0), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2(-1,  1), ipart, fpart));

	//float mylife = abs(sin(time * 0.5)); // XXX: useful for dev
    min_dist = min(max(0.0, min_dist + saturate_field), 1.0);
	min_dist *= step(0.001, life); // disable the shield when under 0.001
	min_dist -= (1.0 - life) * 0.5;

    float blending = min_dist * cellular_base;

	vec2 shield_size = vec2(11.35, 2.8);
	vec2 shield_offset = vec2(0.0, -2.8);
	float border = sd_border((vertP.xy + shield_offset) / shield_size);

	float damage_effect = smoothstep(0.0, 1.0, time - damaging);
    vec3 main_color = mix(vec3(1, 0.16, 0.18), vec3(0.18,0.26,1), damage_effect);
    o_Target = vec4(main_color * (0.0 + blending), 1.0 * border * blending);
    uv *= 0.08;
    o_Target.rgb *= dot(
            sin(-uv * vec2(cos(uv.x*1.), 5.)
                +time*anim_shadow),
            vec2(.7, .55974)
           )*1.0+3.;
	o_Target.rgb = color_reduce(o_Target.rgb);
}

#version 450

layout(location = 0) in vec3 Vertex_Position;

layout(set = 0, binding = 0) uniform CameraViewProj {
    mat4 ViewProj;
    mat4 InverseView;
    mat4 Projection;
    vec3 WorldPosition;
    float near;
    float far;
    float width;
    float height;
};

layout(set = 2, binding = 0) uniform Mesh {
    mat4 Model;
    mat4 InverseTransposeModel;
    uint flags;
};

layout(location = 0) out vec3 vertP;

void main() {
	vec4 P = Model * vec4(Vertex_Position, 1.0);
    gl_Position = ViewProj * P;
	vertP = P.xyz;
}

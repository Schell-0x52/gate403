#version 450

layout(location = 0) in vec3 vertP;
layout(location = 0) out vec4 o_Target;
layout(set = 1, binding = 0) uniform ShieldMaterial {
    float time;
    float damaging;
    float life;
};

// TODO: Make this more pixelized (the game's style will use pseudo pixel art..)

/*/ tristanal
const float global_rotation = 0.0; // diamond: 45.0 and cell_displacement_horizontal = 0.0
const float global_scale = 8.0;
const float scan_line_speed = 50.0;
const float scan_line_freq = 0.004;
const float scan_line_size = 0.99;
const float cellular_intensity = 0.8;
const float cellular_base = 0.5;
const float cellular_anim = 0.9;
const float min_dist_pow = 1.8;

const float cell_displacement_horizontal = 0.0; //0.0 => square, 1.0 => hexagone
const float cell_displacement_amplitude = 1.0;

const float saturate_field = 0.1;
const float invert_color = 0.0;
const float color_thick = 1.0;
const float color_gradiant = 1.0;

const float pixelize = 1.0;
//*/

//*/ hexagonal
const float global_rotation = 0.0; // diamond: 45.0 and cell_displacement_horizontal = 0.0
const float global_scale = 2.0;
const float scan_line_speed = 60.0;
const float scan_line_freq = 0.006;
const float scan_line_size = 0.99;
const float cellular_intensity = 1.0;
const float cellular_base = 1.0;
const float cellular_anim = 0.9;
const float min_dist_pow = 1.5;
const float cell_displacement_horizontal = 0.5; //0.0 => square, 1.0 => hexagone
const float cell_displacement_amplitude = 0.08;

const float saturate_field = 0.2;
const float invert_color = 0.1;
const float color_thick = 1.0;
const float color_gradiant = 1.0;

const float pixelize = 1.0;
//*/

/*/ pixelartal
const float global_rotation = 0.0; // diamond: 45.0 and cell_displacement_horizontal = 0.0
const float global_scale = 64.0;
const float scan_line_speed = 50.0;
const float scan_line_freq = 0.004;
const float scan_line_size = 0.99;
const float cellular_intensity = 0.4;
const float cellular_base = 0.8;
const float cellular_anim = 0.9;
const float min_dist_pow = 1.8;
const float cell_displacement_horizontal = 0.0; //0.0 => square, 1.0 => hexagone
const float cell_displacement_amplitude = 0.5;

const float saturate_field = 0.1;
const float invert_color = 0.4;
const float color_thick = 0.9;
const float color_gradiant = 0.8;

const float pixelize = 8.0;
//*/

vec2 random2( vec2 p ) {
    return
        fract(
            sin(
                vec2(dot(p,vec2(127.1,311.7)),
                     dot(p,vec2(269.5,183.3)))
            )*43758.5453
        );
}

mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}

// Function from Iñigo Quiles
// iquilezles.org/articles/functions
// TODO: Test replacing this by two smoothsteps() for perfs.
float pcurve( float x, float a, float b )
{
    float k = pow(a+b,a+b)/(pow(a,a)*pow(b,b));
    return k*pow(x,a)*pow(1.0-x,b);
}

float sqrlen(vec2 v) {
    return v.x*v.x + v.y*v.y;
}

float dist_neighbor(vec2 neighbor, vec2 ipart, vec2 fpart) {
	vec2 point = vec2(0.0);
	point.x += mod(ipart.y + neighbor.y, 2.0) * cell_displacement_horizontal;
	vec2 target = 0.5 + 0.5*sin(time * cellular_anim + 6.2831*(random2(neighbor + ipart) + point));
	point = mix(point, target, cell_displacement_amplitude);
	return length(neighbor + point - fpart);
}

float sdBox( in vec2 p, in vec2 b ) {
    vec2 d = abs(p)-b;
    return length(max(d,0.0)) + min(max(d.x,d.y),0.0);
}

float sdBorder( in vec2 p ) {
	float rounded = 0.01;
	float border = abs(sdBox(p, vec2(1.0 - rounded)) - rounded);// - 1.0;
	return min(border * 3.0, 1.0);
}


void main()
{

    // "normalized" pixel coordinates (from -1 to 1)
    //vec2 coord = floor(vertP.xy / pixelize)*pixelize;
	//vec2 uv = floor(vertP.xy / pixelize) * pixelize;;
	vec2 uv = vertP.xy;

    uv *= global_scale; // scale

    vec2 uvr = rotate2d((global_rotation * 3.141592)/180.) * uv;

    // Cellular Noise
    vec2 ipart = floor(uvr);
    vec2 fpart = fract(uvr);

    float min_dist = 3.402823466e+38;
	// note GL ES (2.0?) does not support for loops correctly...
	min_dist = min(min_dist, dist_neighbor(vec2( 1, -1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 1,  0), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 1,  1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 0, -1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 0,  0), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2( 0,  1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2(-1, -1), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2(-1,  0), ipart, fpart));
	min_dist = min(min_dist, dist_neighbor(vec2(-1,  1), ipart, fpart));


    // TODO: improve this modulation
    min_dist = pow(min_dist, min_dist_pow);
    min_dist = min(max(0.0, min_dist + saturate_field), 1.0);
    min_dist = invert_color * (1.0 - min_dist) + (1.0 - invert_color) * min_dist;
    min_dist = smoothstep(color_thick-color_gradiant, color_thick, min_dist);
    //min_dist = smoothstep(color_pos-color_thick-color_gradiant, color_pos-color_thick, min_dist);
             //;- smoothstep(pos+thick, pos+thick+gradiant, min_dist);


	float mylife = 0.0;
	min_dist -= mylife;

    float scan = fract((-uv.y + time * scan_line_speed) * scan_line_freq) * scan_line_size;
    //float scan_line = 1.0;//max(0.0, pcurve(scan, 1.0, 3.0));
    float scan_line = max(0.0, pcurve(scan, 1.0, 3.0));
    float blending = scan_line * min_dist * cellular_intensity + min_dist * cellular_base;

	vec2 shield_size = vec2(11.35, 2.8);
	vec2 shield_offset = vec2(0.0, -2.8);
	float border = sdBorder((vertP.xy + shield_offset) / shield_size);

    //vec3 main_color = vec3(0.48,0.56,1);
    vec3 main_color = vec3(0.18,0.26,1);
    o_Target = vec4(main_color * (0.0 + blending), 1.0 * border * blending);
}

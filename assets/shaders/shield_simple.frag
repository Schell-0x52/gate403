#version 450

layout(location = 0) in vec3 vertP;
layout(location = 0) out vec4 o_Target;
layout(set = 1, binding = 0) uniform ShieldMaterial {
    float time;
    float damaging;
    float life;
};


const float pixelize = 8.;
const float colorbit = 32.0;
const float anim_shadow = 2.0;
const float grid_size = 8.0;
const float sd_life = -1.0;
const float global_scale = 0.6;

vec3 pixelcolor(vec3 c) {
    return floor(c * colorbit) / colorbit;
}

// OPTI: image
float sdBox( in vec2 p, in vec2 b ) {
    vec2 d = abs(p)-b;
    return length(max(d,0.0)) + min(max(d.x,d.y),0.0);
}

float sdBorder( in vec2 p ) {
	float rounded = 0.05;
	float border = abs(sdBox(p, vec2(1.0 - rounded)) - rounded);// - 1.0;
	return min(border * 3.0, 1.0);
}

void main() {

	vec3 P = vertP;
    // Normalized pixel coordinates (from -1 to 1)
    //vec2 coord = floor(fragCoord / pixelize) * pixelize;
	vec2 iResolution = vec2(8., 4.);
    vec2 coord = (2.0 * P.xy - iResolution.xy) / iResolution.y;
	vec2 uv = coord * global_scale;


    vec2 grid = uv * grid_size; // grid size

    vec2 ipart = floor(grid);
    vec2 fpart = fract(grid);

    fpart = floor(fpart * pixelize) / pixelize;
    grid = fpart * 2.0 - vec2(1.);
    float box = sdBox(grid, vec2(0.5));
	float damage_effect = smoothstep(0.0, 1.0, time - damaging);

    box = 1.0 - box;
	float mylife = 1.0;
    box -= (1.0 - mylife);

    box = min(max(box, 0.0), 1.0);

    // Time varying pixel color
    vec4 col = mix(vec4(.8, .125, .1, 0.25), vec4(.1, .125, .8, 0.25), damage_effect) * 1.0 * box;


    uv *= 0.5;
    float t = time;
    col *= damage_effect*dot(
            sin(-uv * vec2(cos(uv.x*1.), 5.)
                +t*anim_shadow),
            vec2(.7, .55974)
           )*1.0+3.;

    // Output to screen
	vec2 shield_size = vec2(11.35, 5.6);
	float border = sdBorder(P.xy / shield_size);
    o_Target = vec4(pixelcolor(col.rgb), col.a*border);// * life);
	//o_Target = vec4((P.xy + vec2(1.0)) / 2.0, .0, 1.0);
}

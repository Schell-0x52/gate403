# List Of Detected Bugs

## BUG-0001 [COLLECTING]
### commit 29d63d3
### context
killing orks using both railguns

### log
```
    Finished dev [unoptimized + debuginfo] target(s) in 0.13s
     Running `target/debug/gate403`
The application panicked (crashed).
Message:  Entity does not exist
Location: /home/<name>/.cargo/registry/src/github.com-1ecc6299db9ec823/bevy_ecs-0.5.0/src/world/mod.rs:214

Backtrace omitted.

Run with RUST_BACKTRACE=1 environment variable to display it.
Run with RUST_BACKTRACE=full to include source snippets.
Segmentation fault (core dumped)
```
### status
- RUST_BACKTRACE=full is now always setted
- waiting to reproduce this issue with more information


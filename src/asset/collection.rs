use bevy::prelude::*;
use bevy_asset_loader::AssetCollection;

#[derive(AssetCollection)]
pub struct Ground {
    #[asset(path = "arena/ground.gltf#Mesh0/Primitive0")]
    pub mesh: Handle<Mesh>,
    #[asset(path = "arena/ground.gltf#Material0")]
    pub material: Handle<StandardMaterial>,
}

#[derive(AssetCollection)]
pub struct Blood {
    #[asset(path = "blood.png")]
    pub texture: Handle<Image>,
}

#[derive(AssetCollection)]
pub struct Ork {
    #[asset(path = "ork/orks.gltf#Mesh8/Primitive0")]
    pub body_mesh: Handle<Mesh>,
    #[asset(path = "ork/orks.gltf#Mesh8/Primitive1")]
    pub weapon_mesh: Handle<Mesh>,
    #[asset(path = "ork/orks.gltf#Material0")]
    pub body_material: Handle<StandardMaterial>,
    #[asset(path = "ork/orks.gltf#Material1")]
    pub weapon_material: Handle<StandardMaterial>,
}

#[derive(AssetCollection)]
pub struct Turret {
    #[asset(path = "arena/turret_base.gltf#Mesh0/Primitive0")]
    pub base_mesh: Handle<Mesh>,
    #[asset(path = "arena/turret_cannon.gltf#Mesh0/Primitive0")]
    pub cannon_mesh: Handle<Mesh>,
    #[asset(path = "arena/turret_accum.gltf#Mesh0/Primitive0")]
    pub accum_mesh: Handle<Mesh>,
    #[asset(path = "arena/turret_base.gltf#Material0")]
    pub material: Handle<StandardMaterial>,
    #[asset(path = "arena/turret_base.gltf#Texture0")]
    pub texture: Handle<Image>,
}

#[derive(AssetCollection)]
pub struct Arena {
    #[asset(path = "arena/ground.gltf#Mesh0/Primitive0")]
    pub ground_mesh: Handle<Mesh>,
    #[asset(path = "arena/arena.gltf#Mesh0/Primitive0")]
    pub wall_mesh: Handle<Mesh>,
    #[asset(path = "arena/shield.gltf#Mesh0/Primitive0")]
    pub shield_mesh: Handle<Mesh>,
    #[asset(path = "arena/arena.gltf#Material0")]
    pub material: Handle<StandardMaterial>,
    #[asset(path = "arena/turret_base.gltf#Texture0")]
    pub texture: Handle<Image>,
}

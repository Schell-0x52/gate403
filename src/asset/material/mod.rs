mod blood;
mod bullet;
mod shield;

pub use blood::Material as Blood;
pub use bullet::Material as Bullet;
pub use shield::Material as Shield;

use bevy::ecs::system::lifetimeless::SRes;
use bevy::ecs::system::SystemParamItem;
use bevy::pbr::MaterialPipeline;
use bevy::pbr::SpecializedMaterial;
use bevy::prelude::*;
use bevy::reflect::TypeUuid;
use bevy::render::mesh::MeshVertexBufferLayout;
use bevy::render::render_asset::PrepareAssetError;
use bevy::render::render_asset::RenderAsset;
use bevy::render::render_resource::std140::AsStd140;
use bevy::render::render_resource::std140::Std140;
use bevy::render::render_resource::*;
use bevy::render::renderer::RenderDevice;
use bevy_inspector_egui::Inspectable;

#[derive(Clone, TypeUuid, Inspectable)]
#[uuid = "1e08866c-0b8a-437e-8bce-37733b25127e"]
pub struct Material {
    pub time: f32,
    pub damaging: f32,
    pub life: f32,
}

impl Default for Material {
    fn default() -> Self {
        Material {
            time: 0.0,
            damaging: -100.0,
            life: 1.0,
        }
    }
}

#[derive(Clone)]
pub struct GpuMaterial {
    _buffer: Buffer,
    bind_group: BindGroup,
}

/// The GPU representation of the uniform data
#[derive(Clone, AsStd140)]
pub struct MaterialUniformData {
    pub time: f32,
    pub damaging: f32,
    pub life: f32,
}

impl RenderAsset for Material {
    type ExtractedAsset = Material;
    type PreparedAsset = GpuMaterial;
    type Param = (SRes<RenderDevice>, SRes<MaterialPipeline<Self>>);
    fn extract_asset(&self) -> Self::ExtractedAsset {
        self.clone()
    }

    fn prepare_asset(
        material: Self::ExtractedAsset,
        (render_device, pipeline): &mut SystemParamItem<Self::Param>,
    ) -> Result<Self::PreparedAsset, PrepareAssetError<Self::ExtractedAsset>> {
        let value = MaterialUniformData {
            time: material.time,
            damaging: material.damaging,
            life: material.life,
        };
        let value_std140 = value.as_std140();

        let buffer = render_device.create_buffer_with_data(&BufferInitDescriptor {
            label: Some("bullet_material_uniform_buffer"),
            usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
            contents: value_std140.as_bytes(),
        });

        let bind_group = render_device.create_bind_group(&BindGroupDescriptor {
            entries: &[BindGroupEntry {
                binding: 0,
                resource: buffer.as_entire_binding(),
            }],
            label: Some("bullet_material_bind_group"),
            layout: &pipeline.material_layout,
        });
        Ok(GpuMaterial {
            _buffer: buffer,
            bind_group,
        })
    }
}

impl SpecializedMaterial for Material {
    type Key = ();

    fn key(_: &<Material as RenderAsset>::PreparedAsset) -> Self::Key {}

    fn specialize(
        descriptor: &mut RenderPipelineDescriptor,
        _: Self::Key,
        _layout: &MeshVertexBufferLayout,
    ) -> Result<(), SpecializedMeshPipelineError> {
        descriptor.vertex.entry_point = "main".into();
        descriptor.fragment.as_mut().unwrap().entry_point = "main".into();
        Ok(())
    }

    fn vertex_shader(asset_server: &AssetServer) -> Option<Handle<Shader>> {
        Some(asset_server.load("shaders/shield.vert"))
    }

    fn fragment_shader(asset_server: &AssetServer) -> Option<Handle<Shader>> {
        Some(asset_server.load("shaders/shield.frag"))
    }

    fn alpha_mode(_: &<Self as RenderAsset>::PreparedAsset) -> AlphaMode {
        AlphaMode::Blend
    }

    fn bind_group(render_asset: &<Self as RenderAsset>::PreparedAsset) -> &BindGroup {
        &render_asset.bind_group
    }

    fn bind_group_layout(render_device: &RenderDevice) -> BindGroupLayout {
        render_device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            entries: &[BindGroupLayoutEntry {
                binding: 0,
                visibility: ShaderStages::FRAGMENT,
                ty: BindingType::Buffer {
                    ty: BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: BufferSize::new(
                        MaterialUniformData::std140_size_static() as u64
                    ),
                },
                count: None,
            }],
            label: None,
        })
    }
}

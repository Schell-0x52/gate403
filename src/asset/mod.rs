use bevy::prelude::*;
use bevy::render::view::RenderLayers;

pub mod collection;
pub mod material;

#[derive(Default)]
pub struct ModelHandles {
    pub material: Handle<StandardMaterial>,
    pub mesh: Handle<Mesh>,
    pub transform: Transform,
}

#[derive(Default)]
pub struct BloodHandles {
    pub texture: Handle<Image>,
    pub mesh: Handle<Mesh>,
    pub layer: RenderLayers,
}

#[derive(Default)]
pub struct BulletHandles {
    pub material: Handle<material::Bullet>,
    pub mesh: Handle<Mesh>,
}

#[derive(Default)]
pub struct BulletSphereHandles {
    pub material: Handle<StandardMaterial>,
    pub mesh: Handle<Mesh>,
}

pub type MultiModelHandles = Vec<ModelHandles>;

#[derive(Default)]
pub struct OrkHandles {
    pub firing: MultiModelHandles,
    pub running: [MultiModelHandles; Self::running_framecount()],
}

impl OrkHandles {
    pub const fn running_framecount() -> usize {
        8
    }
}

// XXX This might be better to get them separated
#[derive(Default)]
pub struct AssetHandles {
    pub bullet: BulletHandles,
    pub bullet_sphere: BulletSphereHandles,
    pub ork: OrkHandles,
    pub blood: BloodHandles,
}

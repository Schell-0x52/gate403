use bevy::core::Timer;
use bevy::math::{Quat, Vec3};
use bevy::prelude::*;
use bevy_inspector_egui::Inspectable;
use std::time::Duration;

#[derive(Component, Default)]
pub struct Enemy;
#[derive(Component, Default)]
pub struct Ally;

#[derive(Component)]
pub struct Life(pub f32);

impl Default for Life {
    fn default() -> Self {
        Life(1.0f32)
    }
}

#[derive(Component)]
pub struct FireAtWill;

#[derive(Component)]
pub struct Attack {
    pub target: Vec3,
    pub range: f32,
    pub cooldown: Timer,
}

#[derive(Component)]
pub struct HitBox {
    pub bounding_radius: f32,
}

#[derive(Component)]
pub struct HitDamage {
    pub bounding_radius: f32,
}

#[derive(Component, Default)]
pub struct LiveOneFrame {
    pub alive: bool,
}

#[derive(Component)]
pub struct DespawnOutOfBounds;
#[derive(Component)]
pub struct DespawnOutOfGround;

#[derive(Component)]
pub struct Motion {
    pub velocity: Vec3,
}

#[derive(Component, Default, Inspectable)]
pub struct AnimMeshSheet {
    pub current: usize,
    pub next_frame: Duration,
}

#[derive(Component)]
pub struct FadeInText;

#[derive(Component, Default)]
pub struct AnimMotionConfig {
    pub speed: f32,
    pub scale: f32,
    pub offset: f32,
}

#[derive(Component)]
pub struct ParabolicMotion {
    since: Duration,
    origin: Vec3,
    rotation: Quat,
    config: AnimMotionConfig, // TODO: TMP for parameterization
}

impl ParabolicMotion {
    pub fn build(since: Duration, origin: Vec3, angle_y: f32, config: AnimMotionConfig) -> Self {
        ParabolicMotion {
            since,
            origin,
            rotation: Quat::from_rotation_y(angle_y),
            config,
        }
    }

    pub fn translation(&self, time_since_startup: &Duration) -> Vec3 {
        let t_since = self.since.min(*time_since_startup);
        let t = *time_since_startup - t_since;
        let offset_x = self.config.offset; // 0.5 is the top of the parabola
        let x = offset_x + self.config.speed * t.as_secs_f32();
        let offset_y = 4.0 * offset_x * (1.0 - offset_x);
        let y = 4.0 * x * (1.0 - x);
        let progress =
            self.rotation * Vec3::new((x - offset_x) * self.config.scale, y - offset_y, 0.0);
        self.origin + progress
    }
}

pub mod turret {
    use bevy::input::keyboard::KeyCode;
    use bevy::prelude::*;

    #[derive(Component)]
    pub struct UserInput {
        pub left: KeyCode,
        pub right: KeyCode,
        pub fire: KeyCode,
    }
    impl Default for UserInput {
        fn default() -> Self {
            UserInput {
                left: KeyCode::Escape,
                right: KeyCode::Escape,
                fire: KeyCode::Escape,
            }
        }
    }

    #[derive(Component)]
    pub struct AngleLimit {
        pub left: f32,
        pub right: f32,
    }
    impl Default for AngleLimit {
        fn default() -> Self {
            AngleLimit {
                left: (180f32 + 80.0f32).to_radians(),
                right: (180f32 - 80.0f32).to_radians(),
            }
        }
    }

    #[derive(Component, Default)]
    pub struct Cannon {
        pub trigger: bool,
        pub overheat: f32,
    }

    #[derive(Component, Default)]
    pub struct RotatableMount {
        pub trigger: f32,
        pub motion: f32,
        pub angle_limit: AngleLimit,
        pub angle: f32,
    }
}

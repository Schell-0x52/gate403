use rand::Rng;
use std::time::Duration;

use bevy::pbr::NotShadowCaster;
use bevy::prelude::*;

use crate::asset::material;
use crate::asset::AssetHandles;
use crate::asset::BloodHandles;
use crate::asset::BulletHandles;
use crate::asset::BulletSphereHandles;
use crate::component::Ally;
use crate::component::AnimMeshSheet;
use crate::component::AnimMotionConfig;
use crate::component::Attack;
use crate::component::DespawnOutOfBounds;
use crate::component::DespawnOutOfGround;
use crate::component::Enemy;
use crate::component::FadeInText;
use crate::component::HitBox;
use crate::component::HitDamage;
use crate::component::Life;
use crate::component::Motion;
use crate::component::ParabolicMotion;
use crate::GameConfig;

pub fn spawn_ork_blood(
    commands: &mut Commands,
    transform: Transform,
    assets: &BloodHandles,
    blood_materials: &mut ResMut<Assets<crate::material::Blood>>,
    since_time: Duration,
    angle_y: f32,
    motion: AnimMotionConfig,
) {
    let material = blood_materials.add(crate::material::Blood {
        texture: assets.texture.clone(),
        seconds: 0.0,
    });

    let parabolic_motion =
        ParabolicMotion::build(since_time, transform.translation, angle_y, motion);
    commands
        .spawn()
        .insert_bundle(MaterialMeshBundle {
            mesh: assets.mesh.clone(),
            material,
            transform,
            ..Default::default()
        })
        .insert(parabolic_motion)
        .insert(assets.layer)
        .insert(NotShadowCaster)
        .insert(DespawnOutOfGround {});
}

pub fn spawn_turret_bullet(
    commands: &mut Commands,
    config: &Res<GameConfig>,
    material: Handle<material::Bullet>,
    transform: Transform,
    bullet: &BulletHandles,
    sphere: &BulletSphereHandles,
) {
    let velocity = (transform.rotation * -Vec3::Z).normalize() * -config.bullet_speed;

    commands
        .spawn()
        .insert_bundle(MaterialMeshBundle {
            mesh: bullet.mesh.clone(),
            material,
            transform,
            ..Default::default()
        })
        .insert(Motion { velocity })
        .insert(HitDamage {
            bounding_radius: config.bullet_bbox,
        })
        .insert(DespawnOutOfBounds {})
        .insert(NotShadowCaster)
        //.insert(Name::new("Bullet"))
        .insert(Ally)
        .with_children(|parent| {
            parent
                .spawn_bundle(PbrBundle {
                    mesh: sphere.mesh.clone(),
                    material: sphere.material.clone(),
                    transform: Transform::from_scale(Vec3::splat(0.1)),
                    ..Default::default()
                })
                .with_children(|children| {
                    children.spawn_bundle(PointLightBundle {
                        point_light: PointLight {
                            intensity: 1500.0,
                            range: 5.0,
                            shadows_enabled: false,
                            color: Color::rgb(0.2, 0.2, 1.0),
                            ..Default::default()
                        },
                        ..Default::default()
                    });
                });
        });
}

pub fn spawn_ork_bullet(
    commands: &mut Commands,
    config: &Res<GameConfig>,
    material: Handle<material::Bullet>,
    transform: Transform,
    model: &BulletHandles,
) {
    let velocity = (transform.rotation * -Vec3::Z).normalize() * -config.bullet_speed;

    commands
        .spawn()
        .insert_bundle(MaterialMeshBundle {
            mesh: model.mesh.clone(),
            material,
            transform,
            ..Default::default()
        })
        .insert(Motion { velocity })
        .insert(HitDamage {
            bounding_radius: config.bullet_bbox,
        })
        .insert(DespawnOutOfBounds {})
        .insert(NotShadowCaster)
        //.insert(Name::new("Bullet"))
        .insert(Enemy);
}

pub fn spawn_ork(
    commands: &mut Commands,
    elapsed: &Duration,
    config: &GameConfig,
    assets: &AssetHandles,
    translation: Vec3,
) {
    let transform = Transform::from_translation(translation);

    let velocity = (transform.rotation * Vec3::Z).normalize() * config.ork_speed;
    let mut rng = rand::thread_rng();

    commands
        .spawn()
        .insert(GlobalTransform::default())
        .insert(transform)
        .insert(Motion { velocity })
        .insert(Attack {
            target: Vec3::Z * config.ork_target,
            range: config.ork_firerange,
            cooldown: Timer::from_seconds(config.ork_cooldown, false),
        })
        .insert(HitBox {
            bounding_radius: config.ork_bbox,
        })
        .insert(Enemy {})
        .insert(Life::default())
        .with_children(|parent| {
            parent
                .spawn()
                .insert(Name::new("Running"))
                .insert(GlobalTransform::default())
                .insert(Transform::default())
                .insert(AnimMeshSheet {
                    current: rng.gen_range(0..assets.ork.running.len()),
                    next_frame: *elapsed
                        + Duration::from_secs_f32(rng.gen::<f32>() * config.ork_anim_speed),
                })
                .with_children(|parent| {
                    for pose in assets.ork.running.iter() {
                        parent
                            .spawn()
                            .insert(GlobalTransform::default())
                            .insert(Transform::default())
                            .with_children(|parent| {
                                for model in pose.iter() {
                                    parent.spawn_bundle(PbrBundle {
                                        mesh: model.mesh.clone(),
                                        material: model.material.clone(),
                                        visibility: Visibility { is_visible: false },
                                        ..Default::default()
                                    });
                                }
                            });
                    }
                });
        });
}

pub fn spawn_ending_text(ending_text: &str, commands: &mut Commands, assets: &Res<AssetServer>) {
    commands
        .spawn_bundle(NodeBundle {
            style: Style {
                size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
                position_type: PositionType::Absolute,
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            color: Color::NONE.into(),
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    text: Text::with_section(
                        ending_text,
                        TextStyle {
                            font: assets.load("fonts/font.ttf"),
                            color: Color::rgb(0.92, 0.9, 0.75),
                            font_size: 50.0,
                        },
                        Default::default(),
                    ),
                    ..Default::default()
                })
                .insert(FadeInText {});
        });
}

pub fn spawn_blood_trail(
    commands: &mut Commands,
    transform: &Transform,
    handles: &AssetHandles,
    blood_materials: &mut ResMut<Assets<material::Blood>>,
    since: Duration,
    angle_y: f32,
    orig_motion: &crate::component::AnimMotionConfig,
) {
    let mut rng = rand::thread_rng();
    for i in 0..2 {
        let motion = crate::component::AnimMotionConfig {
            scale: orig_motion.scale - 1.0 * i as f32,
            ..*orig_motion
        };
        let time_offset = Duration::from_millis(10 * i as u64);
        let subtransform = Transform {
            scale: transform.scale - Vec3::ONE * (0.1 * (i) as f32),
            rotation: Quat::from_rotation_y(rng.gen::<f32>() * std::f32::consts::PI * 2.0),
            ..*transform
        };
        spawn_ork_blood(
            commands,
            subtransform,
            &handles.blood,
            blood_materials,
            since + time_offset,
            angle_y,
            motion,
        );
    }
}

pub fn spawn_blood_splash(
    commands: &mut Commands,
    config: &Res<GameConfig>,
    handles: &Res<AssetHandles>,
    blood_materials: &mut ResMut<Assets<material::Blood>>,
    since: &Duration,
    translation: Vec3,
    spread_dir: f32,
) {
    let mut rng = rand::thread_rng();
    let trans_scales: [f32; 3] = [1.5, 3.0, 4.0];
    let motion_scales: [f32; 3] = [5.0, 2.0, 1.0];
    for i in 0..3 {
        let time_offset = Duration::from_millis(rng.gen_range(0..100) as u64);
        let deg_offset = 15f32 * (rng.gen::<f32>() - 0.5);

        let scale = config.blood_scale / motion_scales[i];
        let half_speed = config.blood_speed / 2.0;
        let motion = crate::component::AnimMotionConfig {
            scale, // * rng.gen::<f32>(),
            speed: half_speed + half_speed * rng.gen::<f32>(),
            offset: config.blood_offset,
        };

        let pos_offset =
            ((Vec3::new(rng.gen(), 1f32 + rng.gen::<f32>(), rng.gen()) * 2.0) - Vec3::ONE) * 1.0;

        let transform = Transform {
            translation: translation + pos_offset,
            scale: Vec3::ONE * 1.0 / trans_scales[i],
            ..Default::default()
        };

        spawn_blood_trail(
            commands,
            &transform,
            handles,
            blood_materials,
            *since + time_offset,
            spread_dir + deg_offset.to_radians(),
            &motion,
        );
    }
}

// Based on bevy_assets/filesystem_watcher.rs
use crossbeam_channel::Receiver;
use notify::{Event, RecommendedWatcher, RecursiveMode, Result, Watcher};
use std::path::Path;

/// Watches for changes to assets on the filesystem. This is used by the `AssetServer` to reload
/// them
pub struct FilesystemWatcher {
    pub receiver: Receiver<Result<Event>>,
    _watcher: RecommendedWatcher,
}

impl FilesystemWatcher {
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        let pathbuf = path.as_ref().canonicalize().unwrap();
        let dir = path
            .as_ref()
            .parent()
            .expect("watched file require a parent directory"); // https://github.com/notify-rs/notify/pull/166

        let (sender, receiver) = crossbeam_channel::unbounded();
        let mut watcher: RecommendedWatcher = RecommendedWatcher::new(move |res: Result<Event>| {
            if let Ok(message) = res {
                if message.paths.contains(&pathbuf) {
                    sender.send(Ok(message)).expect("Watch event send failure.");
                }
            }
        })
        .expect("Failed to create filesystem watcher.");

        watcher
            .watch(dir.as_ref(), RecursiveMode::NonRecursive)
            .expect("cannot watch file changes");

        FilesystemWatcher {
            receiver,
            _watcher: watcher,
        }
    }
}

use bevy::{
    app::AppExit,
    asset::AssetServerSettings,
    diagnostic::FrameTimeDiagnosticsPlugin,
    pbr::AmbientLight,
    pbr::NotShadowCaster,
    prelude::*,
    reflect::TypeUuid,
    render::camera::PerspectiveProjection,
    render::camera::{ActiveCameras, CameraProjection, ClearAttachments, ClearOp, ScalingMode},
    render::mesh::VertexAttributeValues,
    render::{
        camera::RenderTarget,
        render_resource::{
            Extent3d, TextureDescriptor, TextureDimension, TextureFormat, TextureUsages,
        },
        view::RenderLayers,
    },
};
use bevy_asset_loader::AssetLoader;
use bevy_fly_camera::{FlyCamera, FlyCameraPlugin};
use bevy_inspector_egui::plugin::InspectorWindows;
use bevy_inspector_egui::WorldInspectorParams;
use bevy_inspector_egui::WorldInspectorPlugin;
use bevy_inspector_egui::{Inspectable, InspectableRegistry, InspectorPlugin};
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::{Error, Write};

mod asset;
mod component;
mod entity;
mod filesystem_watcher;
mod objective;
mod render;
mod spawner;
mod system;

use asset::collection;
use asset::material;

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum AppState {
    Loading, // AssetLoader system
    Play,    // Play the game!
}

// TODO: Improve this.
// See if
// A) We split this huge struct in sub-struct: CameraParams, BulletParams,
// OrkParams, TurretParams, ...
// B) We spread most of these constants in components
//   - no great for performances
//   - cannot edit these config at runtime (for existing entities)
#[derive(Serialize, Deserialize, Inspectable)]
pub struct GameConfig {
    pub camera_fly: bool,
    pub camera_lookat: f32,
    pub camera_update: bool,
    pub blood_speed: f32,
    pub blood_scale: f32,
    //    #[inspectable(min = 0.0, max = 0.5)]
    pub blood_offset: f32,
    pub bullet_speed: f32,
    pub bullet_bbox: f32,
    pub light_color: Color,
    pub light_intensity: f32,
    pub light_range: f32,
    pub ork_firerange: f32,
    pub ork_cooldown: f32,
    pub ork_bbox: f32,
    pub ork_damage_shield: f32,
    pub ork_target: f32,
    pub ork_speed: f32,
    pub ork_anim_speed: f32,
    pub ork_spawn: f32,
    pub ork_bullet_spawn: Vec3,
    pub shield_position_z: f32,
    pub timeline_speed: f32,
    pub turret_bullet_y: f32,
    pub turret_bullet_z: f32,
    pub turret_turn_speed: f32,
    pub turret_turn_maxspeed: f32,
    pub turret_overheat_inc: f32,
    pub turret_overheat_dec: f32,
    pub turret_overheat_penalty: f32,
}

impl Default for GameConfig {
    fn default() -> Self {
        GameConfig {
            camera_fly: false,
            camera_lookat: 1.0,
            camera_update: false,
            blood_speed: 4.0,
            blood_scale: 8.0,
            blood_offset: 0.3,
            bullet_speed: 40.0,
            bullet_bbox: 0.6,
            light_color: Color::WHITE,
            light_intensity: 8000.0,
            light_range: 1000.0,
            ork_firerange: 32.0,
            ork_cooldown: 0.25,
            ork_bbox: 1.5,
            ork_damage_shield: 0.001,
            ork_target: 32.0,
            ork_speed: 5.0,
            ork_anim_speed: 0.04,
            ork_spawn: -38.0,
            ork_bullet_spawn: Vec3::new(-0.6, 1.5, 2.5),
            shield_position_z: 22.0,
            timeline_speed: 1.0,
            turret_bullet_y: 2.75,
            turret_bullet_z: 5.75,
            turret_turn_speed: 2.5,
            turret_turn_maxspeed: 1.0,
            turret_overheat_inc: 0.25,
            turret_overheat_dec: 0.5,
            turret_overheat_penalty: 1.5,
        }
    }
}

const RENDER_IMAGE_HANDLE: HandleUntyped =
    HandleUntyped::weak_from_u64(Image::TYPE_UUID, 13378939762009864029);
const SPAWN_FILENAME: &str = "assets/spawn/default_sheet";
const CONFIG_FILENAME: &str = "assets/config.ron";

fn main() {
    color_backtrace::install();

    let spawn_sheet = spawner::load_sheet(SPAWN_FILENAME);
    let config_file = filesystem_watcher::FilesystemWatcher::new(CONFIG_FILENAME);
    let config = match load_config() {
        Ok(conf) => {
            println!("Loading config from '{}'", CONFIG_FILENAME);
            conf
        }
        Err(msg) => {
            eprintln!(
                "Failed to load config from '{}' (error: {})",
                CONFIG_FILENAME, msg
            );
            GameConfig::default()
        }
    };

    let mut app = App::new();

    AssetLoader::new(AppState::Loading)
        .continue_to_state(AppState::Play)
        .with_collection::<collection::Arena>()
        .with_collection::<collection::Blood>()
        .with_collection::<collection::Ground>()
        .with_collection::<collection::Ork>()
        .with_collection::<collection::Turret>()
        .build(&mut app);

    app.add_state(AppState::Loading)
        // Resources
        .insert_resource(WindowDescriptor {
            width: 1280.,
            height: 720.,
            decorations: true,
            resizable: false,
            //mode: bevy::window::WindowMode::Windowed,
            mode: bevy::window::WindowMode::Fullscreen,
            ..Default::default()
        })
        .insert_resource(AssetServerSettings {
            watch_for_changes: true,
            ..Default::default()
        })
        //.insert_resource(Msaa { samples: 4 })
        .insert_resource(objective::Objectives::from(&spawn_sheet))
        .insert_resource(spawner::Timeline::from_sheet(spawn_sheet))
        .insert_resource(Vec::<system::spawn::Blood>::new())
        .insert_resource(config)
        .insert_resource(config_file)
        // Plugins
        .add_plugins(DefaultPlugins)
        .add_plugin(FrameTimeDiagnosticsPlugin::default())
        .add_plugin(WorldInspectorPlugin::new())
        .add_plugin(InspectorPlugin::<GameConfig>::new_insert_manually())
        .add_plugin(FlyCameraPlugin)
        // Materials
        .add_plugin(MaterialPlugin::<asset::material::Blood>::default())
        .add_plugin(MaterialPlugin::<asset::material::Bullet>::default())
        .add_plugin(MaterialPlugin::<asset::material::Shield>::default())
        // Assets
        .init_resource::<asset::AssetHandles>()
        // Systems
        .add_startup_system(setup_perspective_cam)
        .add_system_set(
            SystemSet::on_enter(AppState::Play)
                .label("assets")
                .with_system(setup_assets),
        )
        .add_system_set(
            SystemSet::on_enter(AppState::Play)
                .after("assets")
                .with_system(setup_ui_cam)
                .with_system(setup_blood_cam)
                .with_system(setup_ground_texture)
                .with_system(setup_entities),
        )
        .add_system_set(
            SystemSet::on_update(AppState::Play)
                .label("pre")
                .with_system(input_exit)
                .with_system(system::turret::input),
        )
        .add_system_set(
            SystemSet::on_update(AppState::Play)
                .label("main")
                .after("pre")
                .before("post")
                .with_system(system::motion::translate)
                .with_system(system::motion::anim_mesh_sheet)
                .with_system(system::motion::parabolic_translate)
                .with_system(system::attack::stop_at_range)
                .with_system(system::attack::fire_at_will)
                .with_system(system::attack::hit_shield)
                .with_system(system::attack::hit_enemies)
                .with_system(system::attack::stop_with_attackers)
                .with_system(system::turret::fire)
                .with_system(system::turret::rotate)
                .with_system(system::gui::fadein_text)
                .with_system(spawner::timeline_system)
                .with_system(config_hot_reloading)
                .with_system(system::debug::show_egui)
                .with_system(system::debug::custom_inspector),
        )
        .add_system_set(
            SystemSet::on_update(AppState::Play)
                .label("post")
                .after("main")
                .with_system(system::motion::despawn_out_of_bounds)
                .with_system(system::motion::despawn_out_of_ground)
                .with_system(system::spawn::blood)
                .with_system(system::motion::live_one_frame)
                .with_system(system::objective::check_objectives)
                .with_system(system::turret::cooldown)
                .with_system(system::material::shield)
                .with_system(system::material::blood)
                .with_system(system::material::bullet),
        )
        .add_system_set(
            SystemSet::on_update(AppState::Play)
                .label("post")
                .with_system(system::attack::despawn_died_enemies.exclusive_system()),
        );

    cfg_if::cfg_if! {
        if #[cfg(feature = "godmode")] {
            app.add_system_set(
                SystemSet::on_update(AppState::Play)
                .label("godmode")
                .after("main")
                .before("post")
                .with_system(system::debug::godmode::blood_effect)
                .with_system(system::debug::godmode::spawn_orks)
            );
        }
    }

    app.world
        .get_resource_mut::<WorldInspectorParams>()
        .unwrap()
        .enabled = false;
    app.world
        .get_resource_mut::<InspectorWindows>()
        .unwrap()
        .window_data_mut::<GameConfig>()
        .visible = false;

    let mut registry = app
        .world
        .get_resource_or_insert_with(InspectableRegistry::default);
    registry.register::<component::AnimMeshSheet>();

    render::blood_pass::add(&mut app);

    app.run();
}

fn setup_render_target(images: &mut ResMut<Assets<Image>>) -> RenderTarget {
    let size = Extent3d {
        width: 512,
        height: 512,
        ..Default::default()
    };

    // This is the texure that will be rendered to.
    let mut image = Image {
        texture_descriptor: TextureDescriptor {
            label: None,
            size,
            dimension: TextureDimension::D2,
            format: TextureFormat::Bgra8UnormSrgb,
            mip_level_count: 1,
            sample_count: 1,
            usage: TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST
                | TextureUsages::RENDER_ATTACHMENT,
        },
        ..Default::default()
    };

    // fill image.data with zeroes
    image.resize(size);

    let image_handle = images.set(RENDER_IMAGE_HANDLE, image);
    RenderTarget::Image(image_handle)
}

fn setup_assets(
    assets: Res<AssetServer>,
    blood: Res<collection::Blood>,
    ork: Res<collection::Ork>,
    mut bullet_materials: ResMut<Assets<material::Bullet>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut handles: ResMut<asset::AssetHandles>,
    mut standard_mats: ResMut<Assets<StandardMaterial>>,
) {
    handles.blood = {
        let quad_size = 4.0;
        let xy2xz = Mat4::from_rotation_x(-90.0f32.to_radians());
        let mut mesh = Mesh::from(shape::Quad::flipped(Vec2::new(quad_size, quad_size)));

        // TODO: Try Plane ?
        // TODO: duplicated code
        if let Some(VertexAttributeValues::Float32x3(vertices)) =
            mesh.attribute_mut(Mesh::ATTRIBUTE_POSITION)
        {
            for v in vertices.iter_mut() {
                let p = xy2xz * Vec4::new(v[0], v[1], v[2], 1.0);
                *v = [p.x, p.y, p.z];
            }
        }

        asset::BloodHandles {
            mesh: meshes.add(mesh),
            texture: blood.texture.clone(),
            layer: RenderLayers::layer(0).with(1),
        }
    };

    handles.bullet = {
        // BulletMesh
        let aspect = 4.0 / 6.0;
        let quad_size = 0.6;
        let xy2xz = Mat4::from_rotation_x(-90.0f32.to_radians());
        let mut mesh = Mesh::from(shape::Quad::flipped(Vec2::new(
            quad_size * aspect,
            quad_size,
        )));
        if let Some(VertexAttributeValues::Float32x3(vertices)) =
            mesh.attribute_mut(Mesh::ATTRIBUTE_POSITION)
        {
            for v in vertices.iter_mut() {
                let p = xy2xz * Vec4::new(v[0], v[1] + quad_size / 2.0, v[2], 1.0);
                *v = [p.x, p.y, p.z];
            }
        }
        asset::BulletHandles {
            mesh: meshes.add(mesh),
            material: bullet_materials.add(material::Bullet::default()),
        }
    };

    handles.bullet_sphere = {
        asset::BulletSphereHandles {
            mesh: meshes.add(Mesh::from(shape::UVSphere {
                sectors: 8,
                stacks: 4,
                ..Default::default()
            })),
            material: standard_mats.add(StandardMaterial {
                base_color: Color::rgb(0.5, 0.5, 1.0),
                unlit: true,
                ..Default::default()
            }),
        }
    };

    handles.ork = {
        let firing: asset::MultiModelHandles = vec![
            asset::ModelHandles {
                mesh: ork.body_mesh.clone(),
                material: ork.body_material.clone(),
                ..Default::default()
            },
            asset::ModelHandles {
                mesh: ork.weapon_mesh.clone(),
                material: ork.weapon_material.clone(),
                ..Default::default()
            },
        ];

        const NEW_HANDLE: asset::MultiModelHandles = asset::MultiModelHandles::new();
        let mut running: [asset::MultiModelHandles; asset::OrkHandles::running_framecount()] =
            [NEW_HANDLE; asset::OrkHandles::running_framecount()];
        for (i, running_frame) in running.iter_mut().enumerate() {
            let path = format!("ork/orks.gltf#Mesh{}", i);
            *running_frame = vec![
                asset::ModelHandles {
                    mesh: assets.load((path.clone() + "/Primitive0").as_str()),
                    material: ork.body_material.clone(),
                    ..Default::default()
                },
                asset::ModelHandles {
                    mesh: assets.load((path + "/Primitive1").as_str()),
                    material: ork.weapon_material.clone(),
                    ..Default::default()
                },
            ];
        }

        asset::OrkHandles { firing, running }
    }
}

fn setup_entities(
    mut commands: Commands,
    arena: Res<collection::Arena>,
    turret: Res<collection::Turret>,
    mut ambient_light: ResMut<AmbientLight>,
    mut shield_materials: ResMut<Assets<material::Shield>>,
    mut standard_mats: ResMut<Assets<StandardMaterial>>,
) {
    // XXX: Failed to get a handle from a material and copy the material.
    //      Cannot synchronously load the material to use it immediately.
    let turretaccum_r_mat = standard_mats.add(new_turret_accum_material(turret.texture.clone()));
    let turretaccum_l_mat = standard_mats.add(new_turret_accum_material(turret.texture.clone()));
    commands
        .spawn()
        .insert(GlobalTransform::default())
        .insert(Transform {
            translation: Vec3::new(-20.6, -1.4, 18.2),
            rotation: Quat::from_rotation_y(180f32.to_radians()),
            ..Default::default()
        })
        .with_children(|parent| {
            parent.spawn_bundle(PbrBundle {
                mesh: turret.base_mesh.clone(),
                material: turret.material.clone(),
                ..Default::default()
            });
            parent.spawn_bundle(PbrBundle {
                mesh: turret.cannon_mesh.clone(),
                material: turret.material.clone(),
                ..Default::default()
            });
            parent.spawn_bundle(PbrBundle {
                mesh: turret.accum_mesh.clone(),
                material: turretaccum_l_mat.clone(),
                ..Default::default()
            });
        })
        .insert(component::turret::UserInput {
            left: KeyCode::A,
            right: KeyCode::D,
            fire: KeyCode::W,
        })
        .insert(component::turret::RotatableMount {
            angle_limit: component::turret::AngleLimit {
                left: (180.0f32 + 10f32).to_radians(),
                ..Default::default()
            },
            angle: 180.0f32.to_radians(),
            ..Default::default()
        })
        .insert(component::turret::Cannon::default())
        .insert(turretaccum_l_mat.clone())
        .insert(Name::new("TurretL"));
    commands
        .spawn()
        .insert(GlobalTransform::default())
        .insert(Transform {
            translation: Vec3::new(20.6, -1.4, 18.2),
            rotation: Quat::from_rotation_y(180f32.to_radians()),
            ..Default::default()
        })
        .with_children(|parent| {
            parent.spawn_bundle(PbrBundle {
                mesh: turret.base_mesh.clone(),
                material: turret.material.clone(),
                ..Default::default()
            });
            parent.spawn_bundle(PbrBundle {
                mesh: turret.cannon_mesh.clone(),
                material: turret.material.clone(),
                ..Default::default()
            });
            parent.spawn_bundle(PbrBundle {
                mesh: turret.accum_mesh.clone(),
                material: turretaccum_r_mat.clone(),
                ..Default::default()
            });
        })
        .insert(component::turret::UserInput {
            left: KeyCode::J,
            right: KeyCode::L,
            fire: KeyCode::I,
        })
        .insert(component::turret::RotatableMount {
            angle_limit: component::turret::AngleLimit {
                right: (180.0f32 - 10f32).to_radians(),
                ..Default::default()
            },
            angle: 180.0f32.to_radians(),
            ..Default::default()
        })
        .insert(component::turret::Cannon::default())
        .insert(turretaccum_r_mat.clone())
        .insert(Name::new("TurretR"));

    let ground_texture_handle = RENDER_IMAGE_HANDLE.typed();
    let ground_material_handle = standard_mats.add(StandardMaterial {
        base_color: Color::rgb_u8(215 - 22 * 2, 205 - 27 * 2, 205 - 27 * 2), // bug? entering 215 gives 232
        base_color_texture: Some(ground_texture_handle),
        unlit: false,
        metallic: 0.75,
        perceptual_roughness: 0.85,
        ..Default::default()
    });

    commands
        .spawn_bundle(PbrBundle {
            mesh: arena.ground_mesh.clone(),
            material: ground_material_handle,
            ..Default::default()
        })
        .insert(Name::new("Ground"));

    commands
        .spawn_bundle(PbrBundle {
            mesh: arena.wall_mesh.clone(),
            material: arena.material.clone(),
            transform: Transform {
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Name::new("Wall"));

    commands
        .spawn_bundle(MaterialMeshBundle {
            mesh: arena.shield_mesh.clone(),
            material: shield_materials.add(material::Shield::default()),
            transform: Transform {
                translation: Vec3::new(0.0, 0.0, 20.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(NotShadowCaster)
        .insert(Name::new("Shield"));
    commands
        .spawn_bundle(PointLightBundle {
            point_light: PointLight {
                intensity: 60000.0,
                range: 5000.0,
                shadows_enabled: true,
                ..Default::default()
            },
            transform: Transform::from_xyz(0.0, 28.0, 0.0),
            ..Default::default()
        })
        .insert(Name::new("Light"));

    ambient_light.color = Color::rgb(1.0, 0.25, 0.25);
    ambient_light.brightness = 0.6;
}

fn new_turret_accum_material(texture: Handle<Image>) -> StandardMaterial {
    StandardMaterial {
        base_color: Color::rgb(1.0, 1.0, 1.0),
        base_color_texture: Some(texture),
        perceptual_roughness: 0.5,
        metallic: 0.7,
        metallic_roughness_texture: None,
        reflectance: 0.5,
        normal_map_texture: None,
        double_sided: false,
        occlusion_texture: None,
        emissive: Color::BLACK,
        emissive_texture: None,
        unlit: false,
        ..Default::default()
    }
}

fn load_config() -> Result<GameConfig, String> {
    match std::fs::read_to_string(CONFIG_FILENAME) {
        Err(e) => Err(format!("Cannot load content of the config file: {}", e)),
        Ok(content) => ron::from_str::<GameConfig>(&content)
            .map_err(|e| format!("Invalid content in the config file: {}", e)),
    }
}

fn save_config(config: &GameConfig) {
    match File::create(CONFIG_FILENAME) {
        Ok(mut file) => {
            let serialized =
                ron::ser::to_string_pretty(config, ron::ser::PrettyConfig::default()).unwrap();
            if let Err(e) = file.write_all(serialized.as_bytes()) {
                println!("failed to write to config file ({}).", e);
            }
        }
        Err(e) => println!("failed to create config file ({}).", e),
    }
}

fn input_exit(
    input: Res<Input<KeyCode>>,
    config: Res<GameConfig>,
    mut exit_channel: EventWriter<AppExit>,
) {
    if input.just_pressed(KeyCode::Escape) {
        save_config(&config);
        exit_channel.send(AppExit);
    }
}

fn setup_perspective_cam(mut commands: Commands, config: Res<GameConfig>) {
    let mut persp_camera = PerspectiveCameraBundle::new_3d();
    persp_camera.perspective_projection = PerspectiveProjection {
        fov: 0.25,
        near: 100.0,
        far: 200.0,
        ..Default::default()
    };
    persp_camera.transform = Transform::from_matrix(Mat4::face_toward(
        Vec3::new(0.0, 72.0, 110.0),
        Vec3::new(0.0, 0.0, config.camera_lookat),
        Vec3::new(0.0, 1.0, 0.0),
    ));
    persp_camera.camera.clear.color = ClearOp::Value(Color::BLACK);
    commands
        .spawn_bundle(persp_camera)
        .insert(FlyCamera {
            key_down: KeyCode::LControl,
            key_up: KeyCode::LShift,
            enabled: false,
            ..Default::default()
        })
        .insert(Name::new("Camera"));
}

fn setup_ui_cam(mut commands: Commands) {
    let mut ui_camera = UiCameraBundle::default();
    ui_camera.camera.clear.color = ClearOp::Value(Color::NONE);
    commands.spawn_bundle(ui_camera);
}

fn setup_blood_cam(
    mut commands: Commands,
    mut images: ResMut<Assets<Image>>,
    mut active_cameras: ResMut<ActiveCameras>,
) {
    let camera_rotation =
        Quat::from_euler(EulerRot::XYZ, -std::f32::consts::FRAC_PI_2, 0.0, 0.0).normalize();
    let camera_size = 50.0; // "ground" is a square of dimension 50.0
    let mut camera_projection = bevy::render::camera::OrthographicProjection {
        scale: camera_size / 2.0,
        scaling_mode: ScalingMode::None,
        ..Default::default()
    };
    camera_projection.update(camera_size, camera_size);

    let render_target = setup_render_target(&mut images);
    let mut ortho_cam = OrthographicCameraBundle::new_3d();
    ortho_cam.camera.name = Some(render::blood_pass::CAMERA.to_string());
    ortho_cam.camera.target = render_target;
    ortho_cam.camera.clear = ClearAttachments {
        color: ClearOp::None,
        depth: ClearOp::Value(0f32),
        stencil: ClearOp::None,
    };
    ortho_cam.camera.projection_matrix = camera_projection.get_projection_matrix(); // TODO still required ?
    ortho_cam.camera.depth_calculation = camera_projection.depth_calculation(); // TODO still required ?
    ortho_cam.orthographic_projection = camera_projection;
    ortho_cam.transform = Transform {
        translation: Vec3::new(0.0, 50.0, -14.5658), // Z based on mesh's geometry center
        rotation: camera_rotation,
        scale: Vec3::ONE,
    };

    active_cameras.add(render::blood_pass::CAMERA);
    commands
        .spawn_bundle(ortho_cam)
        .insert(RenderLayers::layer(1))
        .insert(Name::new("GroundRenderTarget"));
}

fn setup_ground_texture(mut commands: Commands, ground: Res<collection::Ground>) {
    // Used to initialize the render target using ground texture
    commands
        .spawn_bundle(PbrBundle {
            mesh: ground.mesh.clone(),
            material: ground.material.clone(),
            transform: Transform::from_xyz(0.0, 1.0, 0.0),
            ..Default::default()
        })
        .insert(component::LiveOneFrame::default())
        .insert(RenderLayers::layer(1))
        .insert(Name::new("InitGroundRenderTarget"));
}

fn config_hot_reloading(
    mut commands: Commands,
    watcher: Res<filesystem_watcher::FilesystemWatcher>,
) {
    use notify::event::EventKind;
    use notify::event::ModifyKind;

    let mut require_update = false;
    loop {
        let message = match watcher.receiver.try_recv() {
            Ok(message) => message.unwrap(),
            _ => break,
        };

        if let EventKind::Create(_) | EventKind::Modify(ModifyKind::Data(_)) = message.kind {
            require_update = true;
        }
    }

    if require_update {
        match load_config() {
            Ok(config) => {
                println!("Updating config from {}", CONFIG_FILENAME);
                commands.insert_resource(config); // overwrite existing GameConfig
            }
            Err(message) => eprintln!("error: {}", message),
        }
    }
}

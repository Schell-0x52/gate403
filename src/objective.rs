use crate::spawner::{Item, Sheet};

#[derive(Copy, Clone, PartialEq)]
pub enum ObjectiveStates {
    InProgress,
    MissionComplete,
    MissionFailed,
}

pub struct Objectives {
    ork_to_kill: i32,
    shield_life: f32,
    state: ObjectiveStates,
}

impl Objectives {
    pub fn from(sheet: &Sheet) -> Self {
        let mut orks = 0;
        for row in sheet {
            for item in row {
                orks += if *item == Item::Ork { 1 } else { 0 };
            }
        }
        Objectives {
            ork_to_kill: orks,
            shield_life: 1.0,
            state: ObjectiveStates::InProgress,
        }
    }

    pub fn ork_killed(&mut self) {
        self.ork_to_kill -= 1;
        if self.ork_to_kill <= 0 {
            self.state = ObjectiveStates::MissionComplete;
        }
    }

    pub fn damage_shield(&mut self, damage: f32) {
        self.shield_life -= damage;
        if self.shield_life <= 0.0 && self.ork_to_kill > 0 {
            self.state = ObjectiveStates::MissionFailed;
        }
    }

    pub fn state(&self) -> ObjectiveStates {
        self.state
    }
}

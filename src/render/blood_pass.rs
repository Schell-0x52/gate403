use bevy::{
    core_pipeline::{draw_3d_graph, node, AlphaMask3d, Opaque3d, Transparent3d},
    prelude::*,
    render::{camera::ActiveCameras, render_graph::RenderGraph},
    render::{
        camera::ExtractedCameraNames,
        render_graph::{NodeRunError, RenderGraphContext, SlotValue},
        render_phase::RenderPhase,
        renderer::RenderContext,
        RenderApp, RenderStage,
    },
};

// The name of the final node of the first pass.
pub const DRIVER: &str = "first_pass_driver";
// The name of the camera that determines the view rendered in the first pass.
pub const CAMERA: &str = "first_pass_camera";

// A node for the first pass camera that runs draw_3d_graph with this camera.
struct CameraDriver;
impl bevy::render::render_graph::Node for CameraDriver {
    fn run(
        &self,
        graph: &mut RenderGraphContext,
        _render_context: &mut RenderContext,
        world: &World,
    ) -> Result<(), NodeRunError> {
        let extracted_cameras = world.get_resource::<ExtractedCameraNames>().unwrap();
        if let Some(camera_3d) = extracted_cameras.entities.get(CAMERA) {
            graph.run_sub_graph(draw_3d_graph::NAME, vec![SlotValue::Entity(*camera_3d)])?;
        }
        Ok(())
    }
}

pub fn add(app: &mut App) {
    let render_app = app.sub_app_mut(RenderApp);

    // This will add 3D render phases for the new camera.
    render_app.add_system_to_stage(RenderStage::Extract, extract_camera_phases);

    let mut graph = render_app.world.get_resource_mut::<RenderGraph>().unwrap();

    // Add a node for the first pass.
    graph.add_node(DRIVER, CameraDriver);

    // The first pass's dependencies include those of the main pass.
    graph
        .add_node_edge(node::MAIN_PASS_DEPENDENCIES, DRIVER)
        .unwrap();

    // Insert the first pass node: CLEAR_PASS_DRIVER -> DRIVER -> MAIN_PASS_DRIVER
    graph
        .add_node_edge(node::CLEAR_PASS_DRIVER, DRIVER)
        .unwrap();
    graph.add_node_edge(DRIVER, node::MAIN_PASS_DRIVER).unwrap();
}

// Add 3D render phases for CAMERA.
fn extract_camera_phases(mut commands: Commands, active_cameras: Res<ActiveCameras>) {
    if let Some(camera) = active_cameras.get(CAMERA) {
        if let Some(entity) = camera.entity {
            commands.get_or_spawn(entity).insert_bundle((
                RenderPhase::<Opaque3d>::default(),
                RenderPhase::<AlphaMask3d>::default(),
                RenderPhase::<Transparent3d>::default(),
            ));
        }
    }
}

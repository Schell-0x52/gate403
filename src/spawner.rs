use std::fs;

use bevy::prelude::*;

use crate::asset::AssetHandles;
use crate::entity::spawn_ork;
use crate::objective::{ObjectiveStates, Objectives};
use crate::GameConfig;

const ROW_SIZE: usize = 15;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Item {
    Nothing,
    Ork,
}

pub type Row = [Item; ROW_SIZE];
pub type Sheet = Vec<Row>;

pub struct Timeline {
    pub sheet: Sheet,
    pub current: f32,
}

impl Timeline {
    pub fn from_sheet(sheet: Sheet) -> Timeline {
        Timeline {
            sheet,
            current: 0.0,
        }
    }
}

pub fn load_sheet(filename: &str) -> Sheet {
    let content = fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("cannot read spawn sheet from {}", filename));
    let lines = content.split('\n');
    let mut spawn_sheet = Vec::<Row>::new();
    for line in lines {
        let mut row = [Item::Nothing; ROW_SIZE];
        for (i, c) in line.chars().into_iter().enumerate() {
            row[i] = match c {
                'X' => Item::Ork,
                _ => Item::Nothing,
            }
        }
        spawn_sheet.push(row);
    }
    spawn_sheet
}

pub fn timeline_system(
    mut commands: Commands,
    mut timeline: ResMut<Timeline>,
    config: Res<GameConfig>,
    objectives: Res<Objectives>,
    time: Res<Time>,
    handles: Res<AssetHandles>,
) {
    if objectives.state() != ObjectiveStates::InProgress {
        return;
    }

    let current = timeline.current.floor() as usize;
    if current >= timeline.sheet.len() {
        return;
    }

    timeline.current += time.delta_seconds() * config.timeline_speed;
    let after = timeline.current.floor() as usize;
    let stages = after.min(timeline.sheet.len()) - current;

    if stages == 0 {
        return;
    }

    let elapsed = time.time_since_startup();
    for stage in 0..stages {
        let line = timeline.sheet[current + stage];
        for (x, item) in line.iter().enumerate() {
            if item == &Item::Ork {
                let pos = Vec3::new((-7.0 + x as f32) * 3.0, 0.0, config.ork_spawn);
                spawn_ork(&mut commands, &elapsed, &config, &handles, pos);
            }
        }
    }
}

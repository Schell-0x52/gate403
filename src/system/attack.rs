use bevy::prelude::*;

use crate::asset::material;
use crate::asset::AssetHandles;
use crate::entity::spawn_ork_bullet;
use crate::objective::ObjectiveStates;
use crate::objective::Objectives;
use crate::GameConfig;

use crate::component::Ally;
use crate::component::Attack;
use crate::component::Enemy;
use crate::component::FireAtWill;
use crate::component::HitBox;
use crate::component::HitDamage;
use crate::component::Life;
use crate::component::Motion;

const ORK_BULLET_SCALE: f32 = 2.0;

pub fn hit_shield(
    mut commands: Commands,
    config: Res<GameConfig>,
    time: Res<Time>,
    damager_q: Query<(Entity, &Transform), With<Enemy>>,
    mut shield: ResMut<Assets<material::Shield>>,
    mut objectives: ResMut<Objectives>,
) {
    let mut hit = false;
    let mut damage = 0.0;
    damager_q.for_each(|(entity, dmg_tr)| {
        if dmg_tr.translation.z >= config.shield_position_z {
            hit = true;
            damage += config.ork_damage_shield;
            commands.entity(entity).despawn_recursive();
        }
    });

    if hit {
        let t = time.seconds_since_startup() as f32;

        let (shield_handle, &_) = shield.iter().next().expect("no shield instancied");
        let mut shield_asset = shield.get_mut(shield_handle).unwrap();
        shield_asset.damaging = t;
        shield_asset.life -= damage;
        objectives.damage_shield(damage);
    }
}

pub fn despawn_died_enemies(
    mut commands: Commands,
    mut objectives: ResMut<Objectives>,
    q: Query<(Entity, &Life), With<Enemy>>,
) {
    q.for_each(|(entity, life)| {
        if life.0 <= 0f32 {
            commands.entity(entity).despawn_recursive();
            // TODO: Use RemovedComponents<T> instead
            // Tried but not working. Retry in bevy 0.6
            objectives.ork_killed();
        }
    });
}

pub fn hit_enemies(
    damager_q: Query<(&HitDamage, &Transform, &Motion), With<Ally>>,
    mut hittable_q: Query<(&HitBox, &Transform, &mut Life), With<Enemy>>,
    mut bloods: ResMut<Vec<crate::system::spawn::Blood>>,
) {
    damager_q.for_each(|(dmg, dmg_tr, motion)| {
        hittable_q.for_each_mut(|(hittable, transform, mut life)| {
            let v = dmg_tr.translation - transform.translation;
            let sqr_len = v.length_squared();
            let min_len = dmg.bounding_radius + hittable.bounding_radius;
            let sqr_min_len = min_len * min_len;
            if sqr_len < sqr_min_len {
                let velocity = motion.velocity;
                let mut angle_y = (-velocity.z / velocity.x).atan(); // tan A = opposite / adjacant
                if velocity.x < 0.0 {
                    angle_y += std::f32::consts::PI;
                }

                life.0 = 0f32;
                bloods.push(crate::system::spawn::Blood {
                    translation: transform.translation,
                    angle: angle_y,
                });
            }
        });
    });
}

fn start_attacking(
    commands: &mut Commands,
    assets: &AssetHandles,
    entity: &Entity,
    children: &Children,
) {
    commands
        .entity(*entity)
        .remove::<Motion>()
        .insert(FireAtWill {});
    for &child in children.iter() {
        commands.entity(child).despawn_recursive();
    }
    commands
        .spawn()
        .insert(Parent(*entity))
        .insert(GlobalTransform::default())
        .insert(Transform::default())
        .with_children(|parent| {
            parent
                .spawn()
                .insert(Name::new("Firing"))
                .insert(GlobalTransform::default())
                .insert(Transform::default())
                .with_children(|parent| {
                    for model in assets.ork.firing.iter() {
                        parent.spawn_bundle(PbrBundle {
                            mesh: model.mesh.clone(),
                            material: model.material.clone(),
                            ..Default::default()
                        });
                    }
                });
        });
}

pub fn stop_with_attackers(
    mut commands: Commands,
    config: Res<GameConfig>,
    assets: Res<AssetHandles>,
    mut moving_q: Query<(Entity, &mut Transform, &Attack, &Children), With<Motion>>,
    firing_q: Query<&Transform, (With<FireAtWill>, Without<Motion>)>,
) {
    let min_sqr_dist = config.ork_bbox.powf(2.0);
    moving_q.for_each_mut(|(m_entity, mut m_transform, m_attack, m_children)| {
        firing_q.for_each(|f_transform| {
            let sqr_dist = m_transform
                .translation
                .distance_squared(f_transform.translation);
            if sqr_dist <= min_sqr_dist {
                start_attacking(&mut commands, &assets, &m_entity, m_children);
                let target_dir = (m_attack.target - m_transform.translation).normalize();
                m_transform.rotation = Quat::from_rotation_arc(Vec3::Z, target_dir);
            }
        });
    });
}

pub fn stop_at_range(
    mut commands: Commands,
    assets: Res<AssetHandles>,
    mut q: Query<(Entity, &mut Transform, &Attack, &Children), With<Motion>>,
) {
    q.for_each_mut(|(entity, mut transform, attack, children)| {
        let sqr_range = attack.range * attack.range;
        let sqr_dist = attack.target.distance_squared(transform.translation);

        if sqr_dist <= sqr_range {
            start_attacking(&mut commands, &assets, &entity, children);
            let target_dir = (attack.target - transform.translation).normalize();
            transform.rotation = Quat::from_rotation_arc(Vec3::Z, target_dir);
        }
    });
}

pub fn fire_at_will(
    mut commands: Commands,
    config: Res<GameConfig>,
    mut bullet_materials: ResMut<Assets<material::Bullet>>,
    time: Res<Time>,
    objectives: Res<Objectives>,
    handles: Res<AssetHandles>,
    mut q: Query<(&Transform, &mut Attack), With<FireAtWill>>,
) {
    q.for_each_mut(|(transform, mut attack)| {
        if objectives.state() != ObjectiveStates::InProgress {
            return;
        }

        attack.cooldown.tick(time.delta());
        if attack.cooldown.finished() {
            let target_dir = (attack.target - transform.translation).normalize();
            let bullet_tr = Transform {
                translation: transform.mul_vec3(config.ork_bullet_spawn),
                rotation: Quat::from_rotation_arc(Vec3::Z, target_dir),
                scale: Vec3::splat(ORK_BULLET_SCALE),
            };

            spawn_ork_bullet(
                &mut commands,
                &config,
                bullet_materials.add(material::Bullet {
                    color: Color::rgb(1.0, 0.996, 0.6),
                    ..Default::default()
                }),
                bullet_tr,
                &handles.bullet,
            );
            attack.cooldown.reset();
        }
    });
}

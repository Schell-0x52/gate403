use bevy::prelude::*;
use bevy::render::camera::Camera;
use bevy::render::camera::CameraProjection;
use bevy::render::camera::OrthographicProjection;
use bevy::render::camera::PerspectiveProjection;
use bevy_fly_camera::FlyCamera;
use bevy_inspector_egui::plugin::InspectorWindows;
use bevy_inspector_egui::WorldInspectorParams;

use crate::GameConfig;

pub fn show_egui(
    input: Res<Input<KeyCode>>,
    mut world: ResMut<WorldInspectorParams>,
    mut customs: ResMut<InspectorWindows>,
) {
    if input.just_pressed(KeyCode::F2) {
        world.enabled = !world.enabled;
        customs.window_data_mut::<GameConfig>().visible =
            !customs.window_data::<GameConfig>().visible;
    }
}

type QueryPerspectiveCam<'a> = QueryState<(
    &'a mut Camera,
    &'a mut FlyCamera,
    &'a mut Transform,
    &'a PerspectiveProjection,
)>;
type QueryOrthographicCam<'a> = QueryState<(
    &'a mut Camera,
    &'a mut Transform,
    &'a OrthographicProjection,
)>;
pub fn custom_inspector(
    config: ResMut<GameConfig>,
    mut q_set: QuerySet<(QueryPerspectiveCam, QueryOrthographicCam)>,
) {
    if config.camera_update {
        q_set
            .q0()
            .for_each_mut(|(mut cam, mut fly, mut transform, proj)| {
                cam.projection_matrix = proj.get_projection_matrix();
                fly.enabled = config.camera_fly;
                //transform.rotation = Quat::from_rotation_x(config.cam_rotation.to_radians());
                let eye = transform.translation;
                transform.clone_from(&Transform::from_matrix(Mat4::face_toward(
                    eye,
                    Vec3::new(0.0, 0.0, config.camera_lookat),
                    Vec3::new(0.0, 1.0, 0.0),
                )));
            });
        q_set.q1().for_each_mut(|(mut cam, _, proj)| {
            cam.projection_matrix = proj.get_projection_matrix();
        });
    }
}

cfg_if::cfg_if! {
    if #[cfg(feature = "godmode")] {

        pub mod godmode {
            use bevy::prelude::*;
            use bevy::input::keyboard::KeyCode;

            use crate::entity::spawn_ork;
            use crate::entity::spawn_blood_splash;
            use crate::GameConfig;
            use crate::asset::material;
            use crate::asset::AssetHandles;


            pub fn spawn_orks(
                mut commands: Commands,
                time: Res<Time>,
                input: Res<Input<KeyCode>>,
                config: Res<GameConfig>,
                handles: Res<AssetHandles>,
                ) {
                if input.just_pressed(KeyCode::O) {
                    let z = Vec3::Z * -22.0;
                    let elapsed = time.time_since_startup();
                    spawn_ork(&mut commands, &elapsed, &config, &handles, z + Vec3::ZERO);
                    //for i in 1..8 {
                    //    let x = i as f32 * 3.0;
                    //    spawn_ork(&mut commands, &elapsed, &config, &handles, z + Vec3::X * x);
                    //    spawn_ork(&mut commands, &elapsed, &config, &handles, z + Vec3::X * -x);
                    //}
                }
            }

            pub fn blood_effect(
                mut commands: Commands,
                config: Res<GameConfig>,
                input: Res<Input<KeyCode>>,
                handles: Res<AssetHandles>,
                mut blood_materials: ResMut<Assets<material::Blood>>,
                time: Res<Time>,
                ) {
                if input.just_pressed(KeyCode::U) {
                    spawn_blood_splash(
                        &mut commands,
                        &config,
                        &handles,
                        &mut blood_materials,
                        &time.time_since_startup(),
                        Vec3::Y * 0.2,
                        0f32,
                        );
                }
            }
        }

    }
}

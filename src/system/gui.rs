use bevy::prelude::*;

use crate::component::FadeInText;

pub fn fadein_text(
    time: Res<Time>,
    mut commands: Commands,
    mut q: Query<(Entity, &mut Text), With<FadeInText>>,
) {
    q.for_each_mut(|(entity, mut text)| {
        for section in text.sections.iter_mut() {
            if let Color::Rgba { alpha, .. } = &mut section.style.color {
                *alpha += 0.8 * time.delta_seconds();
                if *alpha >= 1.0 {
                    *alpha = 1.0;
                    commands.entity(entity).remove::<FadeInText>();
                }
            }
        }
    });
}

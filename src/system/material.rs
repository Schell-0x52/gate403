use bevy::asset::HandleId;
use bevy::prelude::*;

use crate::asset::material;

pub fn shield(time: Res<Time>, mut material: ResMut<Assets<material::Shield>>) {
    let t = time.seconds_since_startup() as f32;
    let handles: Vec<HandleId> = material.iter().map(|(h, &_)| h).collect();

    for handle in handles {
        if let Some(m) = material.get_mut(handle) {
            m.time = t;
        }
    }
}

pub fn blood(time: Res<Time>, mut material: ResMut<Assets<material::Blood>>) {
    let handles: Vec<HandleId> = material.iter().map(|(h, &_)| h).collect();

    for handle in handles {
        if let Some(m) = material.get_mut(handle) {
            m.seconds += time.delta_seconds();
        }
    }
}

pub fn bullet(time: Res<Time>, mut material: ResMut<Assets<material::Bullet>>) {
    let handles: Vec<HandleId> = material.iter().map(|(h, &_)| h).collect();

    for handle in handles {
        if let Some(m) = material.get_mut(handle) {
            m.seconds += time.delta_seconds();
        }
    }
}

use bevy::diagnostic::Diagnostics;
use bevy::diagnostic::FrameTimeDiagnosticsPlugin;
use bevy::prelude::*;
use bevy::utils::Duration;

use crate::component::AnimMeshSheet;
use crate::component::DespawnOutOfBounds;
use crate::component::DespawnOutOfGround;
use crate::component::LiveOneFrame;
use crate::component::Motion;
use crate::component::ParabolicMotion;
use crate::GameConfig;

const AREA_WIDTH: f32 = 65.0;
const AREA_HEIGHT: f32 = 65.0;
const AREA_BOUNDS: f32 = 5.0;

pub fn despawn_out_of_bounds(
    mut commands: Commands,
    query: Query<(Entity, &Transform), With<DespawnOutOfBounds>>,
) {
    let min_x = -AREA_BOUNDS + -AREA_WIDTH / 2.0;
    let max_x = AREA_BOUNDS + AREA_WIDTH / 2.0;
    let min_z = -AREA_BOUNDS + -AREA_HEIGHT / 2.0;
    let max_z = AREA_BOUNDS + AREA_HEIGHT / 2.0;

    query.for_each(|(entity, transform)| {
        let t = &transform.translation;
        if t.x < min_x || t.x > max_x || t.z < min_z || t.z > max_z {
            commands.entity(entity).despawn_recursive();
        }
    });
}

pub fn despawn_out_of_ground(
    mut commands: Commands,
    time: Res<Time>,
    mut query: Query<(Entity, &mut Transform), With<DespawnOutOfGround>>,
) {
    query.for_each_mut(|(entity, mut transform)| {
        if transform.translation.y < 0.1 {
            let mut t = transform.translation;
            // "preserve" a pseudo rendering order display using time
            t.y = 0.0 + (time.seconds_since_startup().fract() / 10.0) as f32; // [0.0; 0.1]
            transform.translation = t;
            commands.entity(entity).despawn_recursive();
            //commands // XXX: Must be valid
            //    .entity(entity)
            //    .remove::<crate::component::DespawnOutOfGround>()
            //    .remove::<crate::component::ParabolicMotion>();
        }
    });
}

pub fn translate(time: Res<Time>, mut query: Query<(&Motion, &mut Transform)>) {
    for (motion, mut transform) in query.iter_mut() {
        transform.translation.x += motion.velocity.x * time.delta_seconds();
        transform.translation.z += motion.velocity.z * time.delta_seconds();
    }
}

pub fn parabolic_translate(time: Res<Time>, mut query: Query<(&ParabolicMotion, &mut Transform)>) {
    for (motion, mut transform) in query.iter_mut() {
        if transform.translation.y >= 0.1 {
            // TODO tmp
            transform.translation = motion.translation(&time.time_since_startup());
        }
    }
}

pub fn anim_mesh_sheet(
    config: Res<GameConfig>,
    time: Res<Time>,
    mut q_anim_root: Query<(&mut AnimMeshSheet, &Children)>,
    q_parents: Query<&Children, With<Parent>>,
    mut q_meshes: Query<&mut Visibility, (With<Parent>, With<Handle<Mesh>>)>,
) {
    let elapsed = time.time_since_startup();
    for (mut anim, frames) in q_anim_root.iter_mut() {
        if anim.next_frame < elapsed {
            // Hide previous frame
            let parent = frames[anim.current];
            if let Ok(children) = q_parents.get(parent) {
                for &child in children.iter() {
                    if let Ok(mut visibility) = q_meshes.get_mut(child) {
                        visibility.is_visible = false;
                    }
                }
            }
            // Go to next frame
            anim.current = (anim.current + 1) % frames.len();
            anim.next_frame = elapsed + Duration::from_secs_f32(config.ork_anim_speed);

            // Show next frame
            let parent = frames[anim.current];
            if let Ok(children) = q_parents.get(parent) {
                for &child in children.iter() {
                    if let Ok(mut visibility) = q_meshes.get_mut(child) {
                        visibility.is_visible = true;
                    }
                }
            }
        }
    }
}

pub fn live_one_frame(
    mut commands: Commands,
    diagnostics: Res<Diagnostics>,
    mut frame: Local<i32>,
    mut q: Query<(Entity, &mut LiveOneFrame)>,
) {
    let frame_count = diagnostics
        .get(FrameTimeDiagnosticsPlugin::FRAME_COUNT)
        .unwrap()
        .value()
        .expect("cannot get value of frame count out of diagnostics") as i32;
    if *frame != 0 && *frame != frame_count {
        // new frame
        for (id, mut one_cycle) in q.iter_mut() {
            if one_cycle.alive {
                commands.entity(id).despawn_recursive();
            }
            one_cycle.alive = true;
        }
    }

    *frame = frame_count;
}

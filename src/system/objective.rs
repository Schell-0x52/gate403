use bevy::prelude::*;

use crate::entity;
use crate::objective::{ObjectiveStates, Objectives};

pub fn check_objectives(
    // TODO: Search if we can remove a system (when no more used)
    mut text_spawned: Local<bool>,
    mut commands: Commands,
    assets: Res<AssetServer>,
    objectives: Res<Objectives>,
) {
    if *text_spawned {
        return;
    }

    let text = match objectives.state() {
        ObjectiveStates::MissionComplete => "MISSION COMPLETE",
        ObjectiveStates::MissionFailed => "MISSION FAILED",
        _ => return,
    };

    entity::spawn_ending_text(text, &mut commands, &assets);
    *text_spawned = true;
}

use bevy::prelude::*;

use crate::asset::material;
use crate::asset::AssetHandles;
use crate::GameConfig;

pub struct Blood {
    pub translation: Vec3,
    pub angle: f32,
}

pub fn blood(
    mut commands: Commands,
    mut spawns: ResMut<Vec<Blood>>,
    config: Res<GameConfig>,
    handles: Res<AssetHandles>,
    mut blood_materials: ResMut<Assets<material::Blood>>,
    time: Res<Time>,
) {
    let time_since_startup = time.time_since_startup();
    for spawn in spawns.iter() {
        crate::entity::spawn_blood_splash(
            &mut commands,
            &config,
            &handles,
            &mut blood_materials,
            &time_since_startup,
            spawn.translation,
            spawn.angle,
        );
    }
    spawns.clear();
}

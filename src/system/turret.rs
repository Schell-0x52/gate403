use bevy::prelude::*;

use crate::asset::material;
use crate::asset::AssetHandles;
use crate::entity::spawn_turret_bullet;
use crate::objective::{ObjectiveStates, Objectives};
use crate::GameConfig;

use crate::component::turret::Cannon;
use crate::component::turret::RotatableMount;
use crate::component::turret::UserInput;

const BULLET_SCALE: f32 = 5.0;

pub fn fire(
    mut commands: Commands,
    config: Res<GameConfig>,
    handles: Res<AssetHandles>,
    mut bullet_materials: ResMut<Assets<material::Bullet>>,
    mut query: Query<(&mut Cannon, &Transform)>,
) {
    for (mut turret, transform) in query.iter_mut() {
        if turret.trigger && turret.overheat < 1.0 {
            let position = (*transform
                * Transform::from_translation(Vec3::new(
                    0.0,
                    config.turret_bullet_y,
                    config.turret_bullet_z,
                )))
            .translation;
            let bullet_transform = Transform {
                translation: position,
                scale: Vec3::ONE * BULLET_SCALE,
                ..*transform
            };
            let bullet_material = bullet_materials.add(material::Bullet {
                color: Color::rgb(0.2, 0.792, 1.0),
                ..Default::default()
            });
            spawn_turret_bullet(
                &mut commands,
                &config,
                bullet_material,
                bullet_transform,
                &handles.bullet,
                &handles.bullet_sphere,
            );
            turret.overheat += config.turret_overheat_inc;
            if turret.overheat > 1.0 {
                turret.overheat = config.turret_overheat_penalty;
            }
        }
        turret.trigger = false;
    }
}

pub fn rotate(
    config: Res<GameConfig>,
    time: Res<Time>,
    mut query: Query<(&mut RotatableMount, &mut Transform)>,
) {
    for (mut turret, mut transform) in query.iter_mut() {
        let direction = turret.trigger;
        let turn = config.turret_turn_speed * time.delta_seconds();
        let motion = turret.motion;
        let moveup = f32::abs(direction);
        let slowdown = 1.0 - moveup;
        turret.motion = moveup * (motion + direction * turn)
            + slowdown * (motion.signum() * f32::max(/*stop at*/ 0.0, motion.abs() - turn));
        turret.motion =
            turret.motion.signum() * turret.motion.abs().min(config.turret_turn_maxspeed);

        if turret.motion != 0.0 {
            turret.angle += turret.motion * time.delta_seconds();
            turret.angle = turret
                .angle
                .clamp(turret.angle_limit.right, turret.angle_limit.left);
            transform.rotation = Quat::from_rotation_y(turret.angle);
        }
    }
}

pub fn input(
    input: Res<Input<KeyCode>>,
    objectives: Res<Objectives>,
    mut query: Query<(&UserInput, &mut Cannon, &mut RotatableMount)>,
) {
    for (user, mut cannon, mut mount) in query.iter_mut() {
        if objectives.state() == ObjectiveStates::MissionFailed {
            mount.trigger = 0.0;
            cannon.trigger = false;
        } else {
            let mut direction = 0.0;
            if input.pressed(user.left) {
                direction -= -1.0;
            }
            if input.pressed(user.right) {
                direction -= 1.0;
            }
            mount.trigger = direction;
            cannon.trigger = input.just_pressed(user.fire);
        }
    }
}

pub fn cooldown(
    config: Res<GameConfig>,
    time: Res<Time>,
    mut q: Query<(&mut Cannon, &Handle<StandardMaterial>)>,
    mut assets: ResMut<Assets<StandardMaterial>>,
) {
    q.for_each_mut(|(mut turret, material_hdl)| {
        if let Some(material) = assets.get_mut(material_hdl) {
            material.emissive = Color::rgb(turret.overheat.powf(2.0), 0.0, 0.0);
        }
        turret.overheat =
            (turret.overheat - config.turret_overheat_dec * time.delta_seconds()).max(0.0);
    });
}
